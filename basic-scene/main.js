const scene = new THREE.Scene();

const geometry = new THREE.BoxGeometry(1, 1, 1);
const material = new THREE.MeshBasicMaterial({ color: 'yellow' });
const mesh = new THREE.Mesh(geometry, material);
scene.add( mesh );

const viewport = { w: window.innerWidth, h: window.innerHeight }

const camera = new THREE.PerspectiveCamera(75, viewport.w / viewport.h);
camera.position.z = 3;
scene.add(camera);

const canvas = document.querySelector(".webgl");
const renderer = new THREE.WebGLRenderer({ canvas });
renderer.setSize(viewport.w, viewport.h);

renderer.render(scene, camera);